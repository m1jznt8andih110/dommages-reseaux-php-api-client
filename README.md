# Requirements

* PHP >= 7
* [Guzzle](https://docs.guzzlephp.org/en/stable/)

# Usage

```php
<?php

require_once("DommagesReseauxApiClient.php");

$apiClient = new DommagesReseauxApiClient();

$apiClient->login();

$equipment = $apiClient->getEquipmentByCoordinates("RA_POTEAU", 46.7537, 2.2124);
```

# Equipment types

## Aerial network (`RA_*`)

* `RA_POTEAU`: Street pole
* `RA_CABLE`: Cable

## Underground network (`RS_*`)

* `RS_CABLE`: Cable
* `RS_TRAPPE`: Manhole cover

## Street furniture

* `MOB_ARMOIRE`: Street cabinet
* `MOB_COFFRET`: Box on street pole / wall
* `CAB_MOBILIER`: Telephone box

# Links

* [dommages-reseaux.orange.fr](https://dommages-reseaux.orange.fr/dist-dommages/app/home)
* [Dommages Réseaux - 1101011.xyz](https://1101011.xyz/com.orange.labs.reseau1013)