<?php

use GuzzleHttp\Client;

class DommagesReseauxApiClient
{
    private Client $client;

    private ?string $token = null;

    private $cookieJar;

    public function __construct(string $token = null) {
        if (!empty($token)) {
            $this->token = $token;
            $this->buildCookieJar();
        }

        $this->client = new Client([
            'base_uri' => 'https://dommages-reseaux.orange.fr',
            'timeout'  => 5.0,
            'headers' => [
                'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.83 Safari/537.36',
                'Referer' => 'https://dommages-reseaux.orange.fr/dist-dommages/app/signaler',
                'Origin' => 'https://dommages-reseaux.orange.fr',
                'Accept' => 'application/json'
            ]
        ]);
    }

    private function buildCookieJar() {
        if (!empty($this->token)) {
            $this->cookieJar = \GuzzleHttp\Cookie\CookieJar::fromArray(['token' => $this->token], 'dommages-reseaux.orange.fr');
        }
    }

    private static function decodeJsonResponse(\Psr\Http\Message\ResponseInterface $response) {
        if ($response->getStatusCode() < 200 || $response->getStatusCode() >= 300) {
            throw new Exception("HTTP status code not OK");
        }

        if (!$response->hasHeader("Content-Type")) {
            throw new Exception("No 'Content-Type' header in response.");
        }

        if ($response->getHeader("Content-Type")[0] != "application/json") {
            throw new Exception("'Content-Type' header in response invalid.");
        }

        $responseJson = json_decode($response->getBody(), true);

        if ($responseJson === NULL) {
            throw new Exception("Response decoding failed: " . json_last_error_msg());
        }

        return $responseJson;
    }

    /**
     * Permet de s'authentifier et d'obtenir un token
     * @param string $username
     * @param string $password
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function login(string $username = "apii0000", string $password = "apii0000") {
        $response = $this->client->post("/api/auth/login", [
            "json" => [
                "username" => $username,
                "password" => $password
            ]
        ]);

        $responseJson = self::decodeJsonResponse($response);

        if (!isset($responseJson['token'])) {
            throw new Exception("Login failed (no token provided in response).");
        }

        $this->token = $responseJson['token'];
        $this->buildCookieJar();

        return true;
    }

    /**
     * Effectue une requete GET
     * @param string $uri
     * @param array $queryParams
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function get(string $uri, array $queryParams = []) {
        $response = $this->client->get($uri, [
            'query' => $queryParams,
            'cookies' => $this->cookieJar
        ]);

        return self::decodeJsonResponse($response);
    }

    /**
     * Effectue une requete POST
     * @param string $uri
     * @param array|null $data
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function post(string $uri, ?array $data = null) {
        $options = [
            'cookies' => $this->cookieJar
        ];

        if (!is_null($data)) {
            $options['json'] = $data;
        }

        $response = $this->client->post($uri, $options);

        return self::decodeJsonResponse($response);
    }

    /**
     * @return mixed
     */
    public function getSelectList() {
        return $this->get("/api/v1/select-list");
    }

    /**
     * Retourne la liste des types de derangement
     * @return mixed
     */
    public function getDerangementType() {
        return $this->get("/api/v1/derangement-type");
    }

    /**
     * Retourne une liste d'adresses à partir de coordonnées
     * @param float $latitude (Exemple: 46.76306)
     * @param float $longitude (Exemple: 2.42472)
     * @return mixed
     */
    public function getAddressesByCoordinates(float $latitude, float $longitude) {
        return $this->get("/api/v1/oras/addresses", [
            "mode" => "geoCoordinates",
            "geoCodeX" => $longitude,
            "geoCodeY" => $latitude,
            "typography" => "RICH CASE LETTER"
        ]);
    }

    /**
     * Retourne une liste d'adresse à partir d'une recherche textuelle
     * @param string $query (Exemple: "31 Rue du Port")
     * @return mixed
     */
    public function getAddressesByQuery(string $query) {
        return $this->get("/api/v1/oras/addresses", [
            "mode" => "fullText",
            "fullText" => $query,
            "typography" => "RICH CASE LETTER"
        ]);
    }

    /**
     * Retourne les équipement d'un certain type à proximité d'un point géographique
     * @param string $type (Exemples: "RA_POTEAU", "RA_CABLE", "RS_CABLE", "RS_TRAPPE",  "MOB_ARMOIRE", "MOB_COFFRET", "CAB_MOBILIER")
     * @param float $latitude (Exemple: 46.76306)
     * @param float $longitude (Exemple: 2.42472)
     * @return mixed
     */
    public function getEquipmentByCoordinates(string $type, float $latitude, float $longitude) {
        return $this->get("/api/v1/equipement/{$type}/around/latitude/{$latitude}/longitude/{$longitude}");
    }

    /**
     * Retourne le token actuellement utilisé
     * @return string|null
     */
    public function getToken() {
        return $this->token;
    }
}